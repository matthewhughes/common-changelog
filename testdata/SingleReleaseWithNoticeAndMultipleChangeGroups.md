# Changelog

## 0.1.0 - 2023-06-25

_first release_

### Added

- Add the first feature
- Add the second feature

### Removed

- Remove the zeroth feature

### Changed

- Change something else

### Fixed

- Fixed a bug I hadn't even created yet
