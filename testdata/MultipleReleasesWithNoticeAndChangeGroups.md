# Changelog

## 0.1.0 - 2023-06-25

_first release_

### Added

- Add the first feature
- Add the second feature

### Removed

- Remove the zeroth feature

### Changed

- Change something else

### Fixed

- Fixed a bug I hadn't even created yet

## 0.2.0 - 2023-06-26	

_second release_

It's like 90% better than the first one!

### Changed

- Change the first feature
- Change the second feature

### Removed

- Removed another feature

### Fixed

- Fixed a bug in the first feature
