module gitlab.com/matthewhughes/common-changelog

go 1.20

require (
	github.com/matthewhughes934/go-cmark v0.2.0
	github.com/stretchr/testify v1.10.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
