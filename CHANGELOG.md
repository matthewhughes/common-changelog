# Changelog

## 0.2.0 - 2023-09-04

### Changed

  - **Breaking** updated module path to
    `gitlab.com/matthewhughes/common-changelog`

## 0.1.1 - 2023-07-15

### Added

  - Add `pre-commit` hook to validate changelogs against Common Changelog

## 0.1.0 - 2023-06-30

Initial release
