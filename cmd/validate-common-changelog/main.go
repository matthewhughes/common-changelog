/*
validate-common-changelog reads a changelog and validates that adheres to the
[Common Changelog style guide]

Usage:

	validate-common-changelog <changelog-file>

[Common Changelog style guide]: https://common-changelog.org/
*/
package main

import (
	"fmt"
	"os"

	cchlog "gitlab.com/matthewhughes/common-changelog/pkg"
)

func main() { //go-cov:skip
	if err := validateChangelog(os.Args...); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func validateChangelog(args ...string) error {
	if len(args) != 2 {
		return fmt.Errorf("Usage: %s <markdown-changelog>", args[0])
	}

	filename := args[1]
	content, err := os.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("Failed reading file %s: %v", filename, err)
	}

	if _, err := cchlog.ParseChangelog(string(content)); err != nil {
		return fmt.Errorf("Failed to parse changelog: %v", err)
	}

	return nil
}
