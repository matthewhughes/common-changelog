package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFailsOnTooFewArgs(t *testing.T) {
	prgName := "validate-common-changelog"
	expectedErr := "Usage: " + prgName + " <markdown-changelog>"

	err := validateChangelog(prgName)
	require.EqualError(t, err, expectedErr)
}

func TestFailsWhenUnableToOpenFile(t *testing.T) {
	prgName := "validate-common-changelog"
	file := t.TempDir()
	expectedErrPrefix := "Failed reading file " + file + ": "

	err := validateChangelog(prgName, file)

	require.Error(t, err)
	require.Equal(t, expectedErrPrefix, err.Error()[:len(expectedErrPrefix)])
}

func TestFailsOnChangeLogError(t *testing.T) {
	prgName := "validate-common-changelog"
	content := "# Not a changelog!\n"
	filename := filepath.Join(t.TempDir(), "README.md")
	expectedErrPrefix := "Failed to parse changelog: "

	require.NoError(t, os.WriteFile(filename, []byte(content), 0o600))

	err := validateChangelog(prgName, filename)

	require.Error(t, err)
	require.Equal(t, expectedErrPrefix, err.Error()[:len(expectedErrPrefix)])
}

func TestSuccessOnValidChangelog(t *testing.T) {
	prgName := "validate-common-changelog"
	content := "# Changelog\n\n## 0.1.0 - 2023-06-22\n\nThe first release!\n"
	filename := filepath.Join(t.TempDir(), "README.md")

	require.NoError(t, os.WriteFile(filename, []byte(content), 0o600))

	require.NoError(t, validateChangelog(prgName, filename))
}
