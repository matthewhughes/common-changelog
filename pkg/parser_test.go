package cchlog

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/matthewhughes934/go-cmark/pkg/cmark"
	"github.com/stretchr/testify/require"
)

var testdataDir string

func init() {
	_, file, _, ok := runtime.Caller(0)
	if !ok {
		panic("Can't get current filename")
	}
	testdataDir = filepath.Join(filepath.Dir(filepath.Dir(file)), "testdata")
}

func TestParseChangelogInvalidTitle(t *testing.T) {
	for _, tc := range []struct {
		desc        string
		changelog   string
		expectedErr string
	}{
		{
			"Empty document",
			"",
			"Error processing title at line 1: a changelog must have a title",
		},
		{
			"first element not heading",
			"Welcome to my changelog!",
			"Error processing title at line 1: the first element of a changelog must be a heading",
		},
		{
			"invalid title",
			"# My Changelog",
			"Error processing title at line 1: invalid changelog title 'My Changelog', must be: 'Changelog'",
		},
		{
			"wrong title level",
			"## Changelog",
			"Error processing title at line 1: the changelog's title must be a top-level heading",
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			_, err := ParseChangelog(tc.changelog)
			require.EqualError(t, err, tc.expectedErr)
		})
	}
}

func TestParseChangelogInvalidReleaseTitle(t *testing.T) {
	for _, tc := range []struct {
		desc               string
		changelog          string
		lineNumber         int
		expectedErrDetails string
	}{
		{
			"no releases",
			"# Changelog\n",
			1,
			"a changelog must have at least one version",
		},
		{
			"releases doesn't start with heading",
			"# Changelog\n\nwelcome to my changelog\n",
			3,
			"a release must start with a title",
		},
		{
			"wrong level title",
			"# Changelog\n\n# 0.1.0 - 2021-01-01\n",
			3,
			"bad release title, should look like: ## <semver-version> - <date>: but the title is at the wrong level, 2 != 1",
		},
		{
			"bad title",
			"# Changelog\n\n## first release!",
			3,
			"bad release title, should look like: ## <semver-version> - <date>: but there are more than 3 parts separated by ' '",
		},
		{
			"too many '-' in title",
			"# Changelog\n\n## 0.1.0 - 2021-01-01 - what a great release!\n",
			3,
			"bad release title, should look like: ## <semver-version> - <date>: but there are more than 3 parts separated by ' '",
		},
		{
			"invalid version",
			"# Changelog\n\n## abc0.1.0 - 2021-01-01\n",
			3,
			"bad release title, should look like: ## <semver-version> - <date>: but the version ('abc0.1.0') is not a valid semver",
		},
		{
			"invalid date",
			"# Changelog\n\n## 0.1.0 - 2021-13-01\n",
			3,
			"bad release title, should look like: ## <semver-version> - <date>: but the date ('2021-13-01') is not in the correct format",
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			expectedErr := fmt.Sprintf(
				"Error processing releases at line %d: %s",
				tc.lineNumber,
				tc.expectedErrDetails,
			)
			_, err := ParseChangelog(tc.changelog)
			require.EqualError(t, err, expectedErr)
		})
	}
}

func TestParseChangelogInvalidRelease(t *testing.T) {
	changelogPrefix := "# Changelog\n\n## 0.1.0 - 2021-02-12\n"

	for _, tc := range []struct {
		desc               string
		release            string
		lineNumber         int
		expectedErrDetails string
	}{
		{
			"no notes",
			"",
			3,
			"a release must have some notes",
		},
		{
			"invalid release start",
			"\n* this is a list\n* not a change group\n",
			5,
			"expected either a change group heading or a new release, but found a list",
		},
		{
			"change category no text",
			"\n### _Added things_\n",
			5,
			"change heading must be a single category",
		},
		{
			"invalid category",
			"\n### Implemented\n",
			5,
			"change category must be one of: [Changed Added Removed Fixed]",
		},
		{
			"no notes",
			"\n### Added\n",
			5,
			"a change group must have a list of changes",
		},
		{
			"notes not a list",
			"\n### Added\n\nhere I discuss what I added as a paragraph",
			5,
			"a change group must have a list of changes",
		},
		{
			"invalid heading level in changes",
			"\n#### Added\n",
			5,
			"releases must begin with a notice or change category, but found a heading of level 4",
		},
		{
			"change category no text in second change group",
			"\n### Added\n\n* First addition\n* Second addition\n\n### _Added things_\n",
			10,
			"change heading must be a single category",
		},
		{
			"invalid start in second change group",
			"\n### Added\n\n* First thing\n\nThis is a paragraph\n",
			9,
			"expected either a change group heading or a new release, but found a paragraph",
		},
		{
			"invalid category in second change group",
			"\n### Added\n\n* First thing\n\n### Implemented\n",
			9,
			"change category must be one of: [Changed Added Removed Fixed]",
		},
		{
			"not notes in second change group",
			"\n### Added\n\n* First thing\n\n### Changed\n",
			9,
			"a change group must have a list of changes",
		},
		{
			"notes not a list in second change group",
			"\n### Added\n\n* First thing\n\n### Changed\n\nhere I discuss what I added as a paragraph",
			9,
			"a change group must have a list of changes",
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			expectedErr := fmt.Sprintf(
				"Error processing releases at line %d: %s",
				tc.lineNumber,
				tc.expectedErrDetails,
			)
			changelog := changelogPrefix + tc.release

			_, err := ParseChangelog(changelog)
			require.EqualError(t, err, expectedErr)
		})
	}
}

func TestParseChangelogInvalidChangeGroup(t *testing.T) {
	changelogPrefix := "# Changelog\n\n## 0.1.0 - 2021-02-12\n"

	for _, tc := range []struct {
		desc               string
		release            string
		lineNumber         int
		expectedErrDetails string
	}{
		{
			"change category no text",
			"\n### _Added things_\n",
			5,
			"change heading must be a single category",
		},
		{
			"invalid category",
			"\n### Implemented\n",
			5,
			"change category must be one of: [Changed Added Removed Fixed]",
		},
		{
			"no notes",
			"\n### Added\n",
			5,
			"a change group must have a list of changes",
		},
		{
			"notes not a list",
			"\n### Added\n\nhere I discuss what I added as a paragraph",
			5,
			"a change group must have a list of changes",
		},
		{
			"invalid heading level in changes",
			"\n#### Added\n",
			5,
			"releases must begin with a notice or change category, but found a heading of level 4",
		},
	} {
		for _, prefixInfo := range []struct {
			name   string
			prefix string
		}{
			{
				"no prefix",
				"",
			},
			{
				"extra change group",
				"\n### Added\n\n* Add first thing\n",
			},
			{
				"extra release",
				"\n### Added\n\n* Add first thing\n## 0.2.0 - 2021-12-04\n",
			},
		} {
			lineNumber := tc.lineNumber + strings.Count(prefixInfo.prefix, "\n")
			changelog := changelogPrefix + prefixInfo.prefix + tc.release
			desc := fmt.Sprintf("%s with %s", tc.desc, prefixInfo.name)

			t.Run(desc, func(t *testing.T) {
				expectedErr := fmt.Sprintf(
					"Error processing releases at line %d: %s",
					lineNumber,
					tc.expectedErrDetails,
				)

				_, err := ParseChangelog(changelog)
				require.EqualError(t, err, expectedErr)
			})
		}
	}
}

// testdata contains pairs of '*.md' and '*.json' files
// the JSON files contain the expected output of parsing the markdown files
func TestParseChangelogValidChangelogs(t *testing.T) {
	entries, err := os.ReadDir(testdataDir)
	require.NoError(t, err)

	for _, entry := range entries {
		if filepath.Ext(entry.Name()) != ".md" {
			continue
		}
		filename := entry.Name()
		expectedOutFilename := filename[:len(filename)-3] + ".json"
		t.Run(filename, func(t *testing.T) {
			expected := Changelog{}
			expectedOutContent, err := os.ReadFile(filepath.Join(testdataDir, expectedOutFilename))
			require.NoError(t, err, "error reading test changelog output file")
			require.NoError(
				t,
				json.Unmarshal(expectedOutContent, &expected),
				"error parsing test changelog output file",
			)

			path := filepath.Join(testdataDir, filename)
			changelogContent, err := os.ReadFile(path)
			require.NoError(t, err, "error reading test changelog")

			got, err := ParseChangelog(string(changelogContent))
			require.NoError(t, err)
			require.Equal(t, expected, *got)
		})
	}
}

func Test_parseParagraph(t *testing.T) {
	for _, content := range []string{
		"`echo foo >/dev/null` that is useful",
		"plain text",
		"_emph_",
		"_nested **strong** emph_",
		"_emph_ plain text **strong**",
		"[link](www.example.com) here",
		`[link](www.example.com "with label") here`,
		"<emph>cool text!</emph>",
	} {
		t.Run(content, func(t *testing.T) {
			paragraph := cmark.NewParser().ParseDocument(content).FirstChild()

			require.Equal(t, content, parseParagraph(paragraph))
		})
	}
}
