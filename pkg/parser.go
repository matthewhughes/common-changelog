package cchlog

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/matthewhughes934/go-cmark/pkg/cmark"
)

type Changelog struct {
	Title    string
	Releases []Release
}

type Release struct {
	Version      string
	Date         *time.Time
	Notice       string
	ChangeGroups []ChangeGroup
}

type ChangeGroup struct {
	Category ChangeCategory
	Changes  []string
}

// https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
var semVerRe = regexp.MustCompile(
	`^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$`,
)

const (
	changelogTitle    = "Changelog"
	releaseDateFormat = "2006-01-02"
)

type ChangeCategory string

const (
	ChangeCategoryChanged  ChangeCategory = "Changed"
	ChangedCategoryAdded   ChangeCategory = "Added"
	ChangedCategoryRemoved ChangeCategory = "Removed"
	ChangedCategoryFixed   ChangeCategory = "Fixed"
)

func ParseChangelog(changelog string) (*Changelog, error) {
	root := cmark.NewParser().ParseDocument(changelog)

	title, next, err := parseChangelogTitle(root)
	if err != nil {
		return nil, fmt.Errorf("Error processing title at line %d: %v", next.GetStartLine(), err)
	}

	releases, next, err := parseReleases(next)
	if err != nil {
		return nil, fmt.Errorf("Error processing releases at line %d: %v", next.GetStartLine(), err)
	}

	return &Changelog{Title: title, Releases: releases}, nil
}

func parseChangelogTitle(rootNode *cmark.Node) (string, *cmark.Node, error) {
	if rootNode == nil || rootNode.FirstChild() == nil {
		return "", rootNode, errors.New("a changelog must have a title")
	}

	title := rootNode.FirstChild()
	if title.GetType() != cmark.NodeTypeHeading {
		return "", title, errors.New("the first element of a changelog must be a heading")
	}

	if title.GetHeadingLevel() != 1 {
		return "", title, errors.New("the changelog's title must be a top-level heading")
	}

	titleText := *title.FirstChild().GetLiteral()
	if titleText != changelogTitle {
		return "", title, fmt.Errorf(
			"invalid changelog title '%s', must be: '%s'",
			titleText,
			changelogTitle,
		)
	}

	return titleText, title, nil
}

func parseReleases(titleNode *cmark.Node) ([]Release, *cmark.Node, error) {
	var releases []Release
	releaseTitle := titleNode.Next()

	if releaseTitle == nil {
		return nil, titleNode, errors.New("a changelog must have at least one version")
	}
	if releaseTitle.GetType() != cmark.NodeTypeHeading {
		return nil, releaseTitle, errors.New("a release must start with a title")
	}

	node := releaseTitle
	for {
		var release *Release
		var err error
		release, node, err = parseRelease(node)
		if err != nil {
			return nil, node, err
		}
		releases = append(releases, *release)
		if node == nil {
			break
		}
	}

	return releases, titleNode, nil
}

func parseRelease(releaseTitle *cmark.Node) (*Release, *cmark.Node, error) {
	version, date, err := parseReleaseTitle(releaseTitle)
	if err != nil {
		return nil, releaseTitle, err
	}

	releaseStart := releaseTitle.Next()
	if releaseStart == nil {
		return nil, releaseTitle, errors.New("a release must have some notes")
	}

	var notice string
	for releaseStart != nil && releaseStart.GetType() == cmark.NodeTypeParagraph {
		notice += parseParagraph(releaseStart) + "\n"
		releaseStart = releaseStart.Next()
	}

	// loop over all change groups
	var changeGroups []ChangeGroup
	for releaseStart != nil {
		if releaseStart.GetType() == cmark.NodeTypeHeading {
			headingLevel := releaseStart.GetHeadingLevel()
			if headingLevel == 2 {
				break
			} else if headingLevel == 3 {
				// avoid ':=' below to stop aliasing 'releaseStart'
				var changeGroup *ChangeGroup
				changeGroup, releaseStart, err = parseChangeGroup(releaseStart)
				if err != nil {
					return nil, releaseStart, err
				}
				changeGroups = append(changeGroups, *changeGroup)
			} else {
				return nil, releaseStart, fmt.Errorf(
					"releases must begin with a notice or change category, but found a heading of level %d",
					releaseStart.GetHeadingLevel(),
				)
			}
		} else {
			return nil, releaseStart, fmt.Errorf(
				"expected either a change group heading or a new release, but found a %s",
				releaseStart.GetTypeString(),
			)
		}
	}
	return &Release{
		Version:      version,
		Date:         date,
		Notice:       notice,
		ChangeGroups: changeGroups,
	}, releaseStart, nil
}

func parseChangeGroup(node *cmark.Node) (*ChangeGroup, *cmark.Node, error) {
	changeCategory := node.FirstChild()
	if changeCategory == nil || changeCategory.GetType() != cmark.NodeTypeText {
		return nil, node, errors.New("change heading must be a single category")
	}

	validCategories := []ChangeCategory{
		ChangeCategoryChanged,
		ChangedCategoryAdded,
		ChangedCategoryRemoved,
		ChangedCategoryFixed,
	}

	rawCategory := changeCategory.GetLiteral()
	var category ChangeCategory
	for _, cat := range validCategories {
		if *rawCategory == string(cat) {
			category = cat
		}
	}

	if category == "" {
		return nil, changeCategory, fmt.Errorf(
			"change category must be one of: %s",
			validCategories,
		)
	}

	changeList := node.Next()
	if changeList == nil || changeList.GetType() != cmark.NodeTypeList {
		return nil, changeCategory, errors.New("a change group must have a list of changes")
	}

	var changes []string
	for changeList != nil && changeList.GetType() == cmark.NodeTypeList {
		for item := changeList.FirstChild(); item != nil; item = item.Next() {
			changes = append(changes, parseParagraph(item.FirstChild()))
		}
		changeList = changeList.Next()
	}

	return &ChangeGroup{Category: category, Changes: changes}, changeList, nil
}

func parseReleaseTitle(title *cmark.Node) (string, *time.Time, error) {
	errorPrefix := "bad release title, should look like: ## <semver-version> - <date>"

	if title.GetHeadingLevel() != 2 {
		return "", nil, fmt.Errorf(
			"%s: but the title is at the wrong level, %d != 1",
			errorPrefix,
			2,
		)
	}

	titleText := *title.FirstChild().GetLiteral()
	parts := strings.Split(titleText, " ")
	if len(parts) != 3 {
		return "", nil, fmt.Errorf(
			"%s: but there are more than 3 parts separated by ' '",
			errorPrefix,
		)
	}

	semVer := parts[0]
	if !semVerRe.MatchString(semVer) {
		return "", nil, fmt.Errorf(
			"%s: but the version ('%s') is not a valid semver",
			errorPrefix,
			parts[0],
		)
	}

	releaseDate, err := time.Parse(releaseDateFormat, parts[2])
	if err != nil {
		return "", nil, fmt.Errorf(
			"%s: but the date ('%s') is not in the correct format",
			errorPrefix,
			parts[2],
		)
	}

	return semVer, &releaseDate, nil
}

func parseParagraph(paragraph *cmark.Node) string {
	return parseText(paragraph.FirstChild())
}

func parseText(textLikeNode *cmark.Node) string {
	content := ""
	if textLikeNode.GetType() == cmark.NodeTypeText ||
		textLikeNode.GetType() == cmark.NodeTypeHTMLInline {
		content = *textLikeNode.GetLiteral()
	} else if textLikeNode.GetType() == cmark.NodeTypeCode {
		content = fmt.Sprintf("`%s`", *textLikeNode.GetLiteral())
	}

	stringers := map[cmark.NodeType]nodeStringer{
		cmark.NodeTypeEmph:   stringerFromDelim("_"),
		cmark.NodeTypeStrong: stringerFromDelim("**"),
		cmark.NodeTypeLink:   linkStringer,
	}

	if textLikeNode.FirstChild() != nil {
		childContent := parseText(textLikeNode.FirstChild())
		if stringer, ok := stringers[textLikeNode.GetType()]; ok {
			content += stringer(textLikeNode, childContent)
		} else { //go-cov:skip
			panic("not handling " + textLikeNode.GetTypeString() + " yet")
		}
	}
	if textLikeNode.Next() != nil {
		content += parseText(textLikeNode.Next())
	}
	return content
}

type nodeStringer func(*cmark.Node, string) string

func stringerFromDelim(delim string) nodeStringer {
	return func(n *cmark.Node, content string) string {
		return fmt.Sprintf("%[1]s%s%[1]s", delim, content)
	}
}

func linkStringer(link *cmark.Node, content string) string {
	if *link.GetTitle() != "" {
		return fmt.Sprintf(`[%s](%s "%s")`, content, *link.GetUrl(), *link.GetTitle())
	}
	return fmt.Sprintf("[%s](%s)", content, *link.GetUrl())
}
